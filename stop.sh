#!/bin/bash
#if $1 prod stop prod.docker-compose.yml
#if $1 rc stop rc.docker-compose.yml
#if $1 dev stop docker-compose.yml
# Check if the docker is running
if ! docker info > /dev/null 2>&1
then
    echo "Docker is not running"
    exit 1
fi

# Check if the argument is passed
if [ -z "$1" ]
then
    echo "Please pass the environment as an argument"
    exit 1
fi

# Check if the argument is valid
if [ "$1" != "prod" ] && [ "$1" != "rc" ] && [ "$1" != "dev" ]
then
    echo "Please pass a valid environment as an argument"
    exit 1
fi

# Stop the docker-compose
if [ "$1" = "prod" ]
then
    docker-compose -f prod.docker-compose.yaml down
elif [ "$1" = "rc" ]
then
    docker-compose -f rc.docker-compose.yaml down
elif [ "$1" = "dev" ]
then
    docker-compose down
fi