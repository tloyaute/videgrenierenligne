<?php

namespace App\Tests;

use App\Controllers\Api;
use App\Models\Articles;
use App\Models\Cities;
use PHPUnit\Framework\TestCase;

class ApiTest extends TestCase
{
    public function testProductsAction()
    {
        $products = $this->createMock(Api::class);

        $article1 = new Articles();
        $article1->name = 'article1';
        $article2 = new Articles();
        $article2->name = 'article2';
        $article3 = new Articles();
        $article3->name = 'article3';
        $articles = [$article1, $article2, $article3];


        $products->method('ProductsAction')
            ->willReturn($articles);

        $productsAction = $products->ProductsAction();

        $articleNames = array_map(function ($article) {
            return $article->name;
        }, $productsAction);

        $this->assertNotEmpty($productsAction);
        $this->assertContains('article1', $articleNames);
    }

    public function testCitiesAction()
    {
        $cities = $this->createMock(Api::class);

        $city1 = new Cities();
        $city1->name = 'city1';
        $city2 = new Cities();
        $city2->name = 'city2';
        $city3 = new Cities();
        $city3->name = 'city3';
        $citys = [$city1, $city2, $city3];

        $cities->method('CitiesAction')
            ->willReturn($citys);

        $citiesAction = $cities->CitiesAction();

        $cityNames = array_map(function ($city) {
            return $city->name;
        }, $citiesAction);

        $this->assertNotEmpty($citiesAction);
        $this->assertContains('city1', $cityNames);
    }
}
