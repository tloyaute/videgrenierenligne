<?php

namespace App\Models;

use App\Utility\Hash;
use Core\Model;
use App\Core;
use Exception;
use App\Utility;
use App\Config;

/**
 * User Model:
 */
class User extends Model {
    /**
     * Crée un utilisateur
     */
    public static function createUser($data) {
        $db = static::getDB();

        $stmt = $db->prepare('INSERT INTO users(username, email, password, salt) VALUES (:username, :email, :password,:salt)');

        $stmt->bindParam(':username', $data['username']);
        $stmt->bindParam(':email', $data['email']);
        $stmt->bindParam(':password', $data['password']);
        $stmt->bindParam(':salt', $data['salt']);

        $stmt->execute();

        return $db->lastInsertId();
    }

    public static function getByLogin($login)
    {
        $db = static::getDB();

        $stmt = $db->prepare("
            SELECT * FROM users WHERE ( users.email = :email) LIMIT 1
        ");

        $stmt->bindParam(':email', $login);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }


    /**
     * ?
     * @access public
     * @return string|boolean
     * @throws Exception
     */
    public static function login() {
        $db = static::getDB();

        $stmt = $db->prepare('SELECT * FROM articles WHERE articles.id = ? LIMIT 1');

        $stmt->execute([$id]);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Create Remember Cookie: Inserts a remember-me hash into database and
     * cookie.
     * @access public
     * @param string $userID
     * @return boolean
     */
    public static function createRememberCookie($userID) {
        $Db = static::getDB();
        $check = $Db->prepare("SELECT * FROM user_cookies WHERE user_id = ?");
        $check->execute([$userID]);
        $data = $check->fetchALL(\PDO::FETCH_ASSOC);
        if (!empty($data)) {
            $hash = $data[0]['hash'];
        } else {
            $hash = Utility\Hash::generateUnique();
            $insert = $Db->prepare("INSERT INTO user_cookies (user_id, hash) VALUES (?, ?)");
            if(!$insert->execute([$userID, $hash])) {
                return false;
            }
        }
        $cookie = Config::USER_COOKIE;
        $expiry = Config::USER_COOKIE_EXPIRY;
        return(Utility\Cookie::put($cookie, $hash, $expiry));
    }

    /**
     * Login With Cookie: Checks if a remember me cookie has been exists and
     * attempts to login the user if the cookie value is found in the database
     * - writing all necessary data into the session if the login was successful.
     * Returns true if everything is okay, otherwise turns false.
     * @access public
     * @return boolean
     */
    public static function loginWithCookie() {

        // Check if a remember me cookie exists.
        if (!Utility\Cookie::exists(Config::USER_COOKIE)) {
            return false;
        }

        // Check if the hash exists in the database, grabbing the ID of the user
        // it is attached to it if it does.
        $Db = static::getDB();
        $hash = Utility\Cookie::get(Config::USER_COOKIE);
        $check = $Db->prepare("SELECT * FROM user_cookies WHERE hash = ?");
        $check->execute([$hash]);
        $data = $check->fetchALL(\PDO::FETCH_ASSOC);
        if (!empty($data)) {
            $userID = $data[0]['user_id'];
            // Check if the user exists.
            $user = $Db->prepare("SELECT * FROM users WHERE id = ?");
            $user->execute([$userID]);
            $userData = $user->fetchALL(\PDO::FETCH_ASSOC);

            if (!empty($userData)) {
                $userData = $userData[0];
                $_SESSION['user'] = array(
                    'id' => $userData['id'],
                    'username' => $userData['username'],
                    'is_admin' => $userData['is_admin'],
                    'email' => $userData['email']
                );
                return true;
            }
        }
        Utility\Cookie::delete(Config::USER_COOKIE);
        return false;
    }

    /**
     * Delete cookie: delete user cookie in database and cookie.
     * @access public
     * @return void
     */
    public static function deleteCookie() {
        $Db = static::getDB();
        $hash = Utility\Cookie::get(Config::USER_COOKIE);
        $delete = $Db->prepare("DELETE FROM user_cookies WHERE hash = ?");
        $delete->execute([$hash]);
        Utility\Cookie::delete(Config::USER_COOKIE);
    }

    public static function getNumberOfUser() {
        $db = static::getDB();

        $stmt = $db->prepare('SELECT COUNT(*) FROM users');

        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_COLUMN, 0);
    }

    public static function getById($id) {
        $db = static::getDB();

        $stmt = $db->prepare('SELECT * FROM users WHERE id = ? LIMIT 1');

        $stmt->execute([$id]);

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public static function IsAdmin($id) {
        $db = static::getDB();

        $stmt = $db->prepare('SELECT is_admin FROM users WHERE id = ? LIMIT 1');

        $stmt->execute([$id]);
        if($stmt->fetch(\PDO::FETCH_COLUMN, 0) == 1) {
            return true;
        } else {
            return false;
        }
    }
}
