<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Utility\Flash;
use App\Utility\Upload;
use \Core\View;

/**
 * Product controller
 */
class Stats extends \Core\Controller
{

    /**
     * Affiche la page de stats
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('stats.html', []);
    }
}
