<?php

namespace App\Controllers;

use App\Config;
use App\Model\UserRegister;
use App\Models\Articles;
use App\Utility\Cookie;
use App\Utility\Flash;
use App\Utility\Hash;
use App\Utility\Session;
use \Core\View;
use Exception;
use http\Env\Request;
use http\Exception\InvalidArgumentException;
/**
 * User controller
 */
class User extends \Core\Controller
{

    /**
     * Affiche la page de login
     */

    /**
     * @OA\Post(
     *     path="/api/login",
     *     @OA\Response(response="200", description="Affiche la page de login"),
     *     @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email de l'utilisateur",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *      )
     *  ),
     *     @OA\Parameter(
     *     name="password",
     *     in="query",
     *     description="Mot de passe de l'utilisateur",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *      )
     *  ),
     * )
     */
    public function loginAction()
    {
        if(isset($_POST['submit'])){
            $f = $_POST;

            // TODO: Validation

            $this->login($f);

            // Si login OK, redirige vers le compte
            header('Location: /account');
        }

        View::renderTemplate('User/login.html');
    }

    /**
     * Page de création de compte
     */

    /**
     * @OA\Post(
     *     path="/api/register",
     *     @OA\Response(response="200", description="Affiche la page de création de compte"),
     *     @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email de l'utilisateur",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *      )
     *  ),
     *     @OA\Parameter(
     *     name="username",
     *     in="query",
     *     description="Nom d'utilisateur",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *      )
     *  ),
     *     @OA\Parameter(
     *     name="password",
     *     in="query",
     *     description="Mot de passe de l'utilisateur",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *      )
     *  ),
     *     @OA\Parameter(
     *     name="password_confirm",
     *     in="query",
     *     description="Confirmation du mot de passe de l'utilisateur",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *      )
     *  ),
     * )
     */
    public function registerAction()
    {
        if(isset($_POST['submit'])){
            $f = $_POST;
            if($this->register($f)){
                $this->login($f);

                header('Location: /account');
            }
        }

        View::renderTemplate('User/register.html');
    }

    /**
     * Affiche la page du compte
     */
    public function accountAction()
    {
        $articles = Articles::getByUser($_SESSION['user']['id']);

        View::renderTemplate('User/account.html', [
            'articles' => $articles
        ]);
    }

    /*
     * Fonction privée pour enregister un utilisateur
     */
    private function register($data)
    {
        try {
            // Generate a salt, which will be applied to the during the password
            // hashing process.
            if($data["email"] == "" || $data["username"] == "" || $data["password"] == "" || $data["password-check"] == ""){
                throw new Exception("Tous les champs sont obligatoires");
            }

            if($data["password"] !== $data["password-check"]){
                throw new Exception("Les mots de passe ne correspondent pas");
            }

            if(\App\Models\User::getByLogin($data['email'])){
                throw new Exception("L'email est déjà utilisé");
            }

            $salt = Hash::generateSalt(32);

            $userID = \App\Models\User::createUser([
                "email" => $data['email'],
                "username" => $data['username'],
                "password" => Hash::generate($data['password'], $salt),
                "salt" => $salt
            ]);

            return $userID;

        } catch (Exception $ex) {
            Flash::danger($ex->getMessage());
        }
    }

    private function login($data){
        try {
            if(!isset($data['email'])){
                throw new Exception('Veuillez renseigner votre email');
            }

            $user = \App\Models\User::getByLogin($data['email']);

            if (Hash::generate($data['password'], $user['salt']) !== $user['password']) {
                return false;
            }


            if(isset($data['remember']) && $data['remember'] == 'on'){
                \App\Models\User::createRememberCookie($user['id']);
            }   

            $_SESSION['user'] = array(
                'id' => $user['id'],
                'username' => $user['username'],
                'is_admin' => $user['is_admin'],
                'email' => $user['email']
            );

            return true;

        } catch (Exception $ex) {
            Flash::danger($ex->getMessage());
        }
    }

    /**
     * Login With Cookie: Processes a login with cookie request. NOTE: This
     * controller can only be accessed by unauthenticated users!
     * @access public
     * @example login/_loginWithCookie
     * @return void
     */
    public function loginWithCookie() {

        // Check that the user is unauthenticated.
        if(isset($_SESSION['user'])){
            header("Location: /");
        }
        // Process the login with cookie request, redirecting to the home
        // controller if successful or back to the login controller if not.
        if (\App\Models\User::loginWithCookie()) {
            header("Location: /");
        } else {
            header("Location: /login");
        }
    }

    /**
     * Logout: Delete cookie and session. Returns true if everything is okay,
     * otherwise turns false.
     * @access public
     * @return boolean
     * @since 1.0.2
     */
    public function logoutAction() {
        // Destroy all data registered to the session.

        // Delete the users remember me cookie if one has been stored.
        $cookie = Config::USER_COOKIE;
        if (Cookie::exists($cookie)) {
            \App\Models\User::deleteCookie($cookie);
        }

        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();

        header ("Location: /");

        return true;
    }

}
