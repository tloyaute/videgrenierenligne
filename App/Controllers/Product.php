<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Utility\Flash;
use App\Utility\Upload;
use \Core\View;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

/**
 * Product controller
 */
class Product extends \Core\Controller
{

    /**
     * Affiche la page d'ajout
     * @return void
     */

    /**
     * @OA\Post(
     *     path="/api/product",
     *     @OA\Response(response="200", description="Ajouter un produit"),
     *     @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Nom du produit",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *    )
     *  ),
     *     @OA\Parameter(
     *     name="description",
     *     in="query",
     *     description="Description du produit",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *   )
     * ),
     *     @OA\Parameter(
     *     name="picture",
     *     in="query",
     *     description="Image du produit",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *   )
     * ),
     * )
     */
    public function indexAction()
    {

        if (isset($_POST['submit'])) {

            try {
                $f = $_POST;
                if ($f['name'] == "" || $f['description'] == "") {
                    throw new \Exception('Tous les champs sont obligatoires');
                }
                if ($_FILES['picture']['error'] != 0) {
                    throw new \Exception('Une erreur est survenue lors de l\'upload de l\'image');
                }

                $f['user_id'] = $_SESSION['user']['id'];
                $id = Articles::save($f);

                $pictureName = Upload::uploadFile($_FILES['picture'], $id);

                Articles::attachPicture($id, $pictureName);

                header('Location: /product/' . $id);
            } catch (\Exception $e) {
                Flash::danger($e->getMessage());
            }
        }

        View::renderTemplate('Product/Add.html');
    }

    /**
     * Affiche la page d'un produit
     * @return void
     */
    public function showAction()
    {
        $id = $this->route_params['id'];

        try {
            Articles::addOneView($id);
            $suggestions = Articles::getSuggest();
            $article = Articles::getOne($id);
        } catch (\Exception $e) {
            Flash::danger($e->getMessage());
        }
        $article[0]['id'] = $id;
        View::renderTemplate('Product/Show.html', [
            'article' => $article[0],
            'suggestions' => $suggestions
        ]);
    }

    /**
     * Envoie de mail pour contacter le vendeur
     * @return void
     */
    public function contactAction()
    {
        $id = $this->route_params['id'];

        try {
            $article = Articles::getOne($id);
            $user = \App\Models\User::getById($article[0]['user_id']);
            $f = $_POST;
            if($f['email'] == "" || $f['message'] == "" || $f['name'] == ""){
                throw new \Exception('Tous les champs sont obligatoires');
            }
            $mail = new PHPMailer();

            try {
                //Server settings
                $mail->isSMTP();
                $mail->Host       = 'smtp.gmail.com';
                $mail->SMTPAuth   = true;
                $mail->Username   = getenv("SMTP_USERNAME");
                $mail->Password   = getenv("SMTP_PASSWORD");
                $mail->Port = 465;
                $mail->SMTPSecure = "ssl";

                //Recipients
                $mail->setFrom(getenv("SMTP_USERNAME"), 'VideGrenierEnLigne');
                $mail->addAddress($user['email'], $user['username']);     //Add a recipient

                //Content
                $mail->isHTML(true);  
                $mail->CharSet = "UTF-8";                              //Set email format to HTML
                $mail->Encoding = 'base64';
                $mail->Subject = 'Vous avez reçu un message pour l\'article ' . $article[0]['name'];
                $mail->Body    = 'De : ' . $f['name'] . ' (' . $f['email'] . ')<br><br>' . $f['message'];
                $mail->AltBody = 'De : ' . $f['name'] . ' (' . $f['email'] . ')<br><br>' . $f['message'];

                $mail->send();
                
                Flash::success('Votre message a bien été envoyé');

                header('Location: /product/' . $id);
            } catch (Exception $e) {
                Flash::danger('Une erreur est survenue lors de l\'envoi du mail');
            }
        } catch (\Exception $e) {
            Flash::danger($e->getMessage());
        }

        header('Location: /product/' . $id);
    }
}
