<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Models\Cities;
use App\Models\User as User;
use \Core\View;
use Exception;
use App\OpenApi\Annotations as OA;

/**
 * API controller
 */
/**
 * @OA\Info(title="Vide de grenier API", version="0.1")
 */
class Api extends \Core\Controller
{

    /**
     * Affiche la liste des articles / produits pour la page d'accueil
     *
     * @throws Exception
     */

    /**
     * @OA\Get(
     *     path="/api/products",
     *     @OA\Response(response="200", description="Affiche la liste des articles / produits pour la page d'accueil"),
     * )
     */
    public function ProductsAction()
    {
        $query = $_GET['sort'];

        $articles = Articles::getAll($query);

        header('Content-Type: application/json');
        echo json_encode($articles);
    }

    /**
     * Recherche dans la liste des villes
     *
     * @throws Exception
     */

    /**
     * @OA\Get(
     *     path="/api/cities",
     *     @OA\Response(response="200", description="Recherche dans la liste des villes"),
     *     @OA\Parameter(
     *     name="query",
     *     in="query",
     *     description="Nom de la ville recherchée",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *    )
     *  )
     * )
     */
    public function CitiesAction(){

        $cities = Cities::search($_GET['query']);

        header('Content-Type: application/json');
        echo json_encode($cities);
    }

    /**
     * Affiche les statistiques
     *
     * @throws Exception
     */

    /**
     * @OA\Get(
     *     path="/api/stats",
     *     @OA\Response(response="200", description="Affiche les statistiques")
     * )
     */
    public function StatsAction(){
        $f['user_id'] = $_SESSION['user']['id'];
        $user = User::getById($f['user_id']);

        if ($user['is_admin'] == 1) {
            $createdArticleToday = Articles::getCreatedToday();
            $createdArticleYesterday = Articles::getCreatedYesterday();
            $createdArticleThisWeek = Articles::getCreatedThisWeek();

            $numberOfUser = User::getNumberOfUser();

            $stats = [
                'createdArticleToday' => $createdArticleToday,
                'createdArticleYesterday' => $createdArticleYesterday,
                'createdArticleThisWeek' => $createdArticleThisWeek,
                'numberOfUser' => $numberOfUser
            ];
            header('Content-Type: application/json');
            echo json_encode($stats);
        } else {
            header('Content-Type: application/json');
            echo json_encode(['error' => 'Vous n\'êtes pas autorisé à accéder à cette page']);
        }
    }
}
