#!/bin/bash
# This script is used to deploy the application to the server
# if $1 = prod then it will pull master branch and run prod.docker-compose.yml
# if $1 = rc then it will pull develop branch and run rc.docker-compose.yml
# if $1 = dev the run docker-compose.yml

# Check if the argument is passed
if [ -z "$1" ]
then
    echo "Please pass the environment as an argument (dev, rc, prod)"
    exit 1
fi

# Check if the argument is valid
if [ "$1" != "prod" ] && [ "$1" != "rc" ] && [ "$1" != "dev" ]
then
    echo "Please pass a valid environment as an argument (dev, rc, prod)"
    exit 1
fi

# Check if the docker is installed
if ! [ -x "$(command -v docker)" ]
then
    echo "Docker is not installed"
    exit 1
fi

# Check if the docker-compose is installed
if ! [ -x "$(command -v docker-compose)" ]
then
    echo "Docker-compose is not installed"
    exit 1
fi

# Check if the docker is running
if ! docker info > /dev/null 2>&1
then
    echo "Docker is not running"
    exit 1
fi

#pull the latest with the branch name
if [ "$1" = "prod" ]
then
    git pull origin master
elif [ "$1" = "rc" ]
then
    git pull origin develop
fi

# Run the docker-compose
if [ "$1" = "prod" ]
then
    docker-compose -f prod.docker-compose.yaml up -d --build
elif [ "$1" = "rc" ]
then
    docker-compose -f rc.docker-compose.yaml up -d --build
elif [ "$1" = "dev" ]
then
    docker-compose up -d --build
fi
